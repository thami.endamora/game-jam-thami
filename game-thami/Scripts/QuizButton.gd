extends TextureButton

export (bool) var click_block = false
var finished_popup

func _on_TextureButton_pressed():
	if click_block == true:
		TaskCounts.bonusDone += 1
		TaskCounts.TreasureChest = true
		finished_popup = get_node("../../../../../../PopupRight")
		finished_popup.show()
		
		if (GlobalTimer.s + 10) >= 60:
			GlobalTimer.m += 1
			GlobalTimer.s = 9
		else:
			GlobalTimer.s += 10
			
		if GlobalTimer.h <= 0 and GlobalTimer.m <= 0 and GlobalTimer.s <= 0:
			get_tree().change_scene(str("res://Scenes/7LosingScreen.tscn"))

	if click_block == false:
		TaskCounts.bonusDone += 1
		TaskCounts.TreasureChest = true
		finished_popup = get_node("../../../../../../PopupWrong")
		finished_popup.show()
		
		if (GlobalTimer.s - 10) <= 0:
			GlobalTimer.m -= 1
			GlobalTimer.s = 49
		else:
			GlobalTimer.s -= 10
			
		if GlobalTimer.h <= 0 and GlobalTimer.m <= 0 and GlobalTimer.s <= 0:
			get_tree().change_scene(str("res://Scenes/7LosingScreen.tscn"))
