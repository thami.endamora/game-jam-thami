extends RichTextLabel

func _process(delta):
	if GlobalTimer.s == 0:
		GlobalTimer.m -= 1
		GlobalTimer.s = 59
	set_text("%02d:%02d:%02d" % [GlobalTimer.h,GlobalTimer.m,GlobalTimer.s])


func _on_Timer_timeout():
	GlobalTimer.s -= 1
	if GlobalTimer.h <= 0 and GlobalTimer.m <= 0 and GlobalTimer.s <= 0:
		get_tree().change_scene(str("res://Scenes/7LosingScreen.tscn"))
