extends Node

var PopUpMenu

func _ready():
	PopUpMenu = get_node("CreditPopup")

func _on_CloseCredit_pressed():
	PopUpMenu.hide()

func _on_CreditsButton_pressed():
	PopUpMenu.show()
