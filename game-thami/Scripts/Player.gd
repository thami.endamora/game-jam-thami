# Thami Endamora - 1806141460 #

extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()

# Var for Double Jump
var jump_count = 0

func get_input():
	var animation = "idle"
	
	velocity.x = 0
	if is_on_floor():
		jump_count = 0 # Reset count after landing on floor
	if jump_count < 2 and Input.is_action_just_pressed('up'): # Double Jump
		velocity.y = jump_speed
		jump_count += 1
	if Input.is_action_pressed('right'):
		velocity.x += speed
		animation = "walk_right"
	if !is_on_floor(): # Jump + Jump right
			animation = "jump_right"
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		animation = "walk_left"
		if !is_on_floor():
			animation = "jump_left"
	
	
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
