extends TextureButton

export(String) var buttonid
var finished_popup

func _ready():
	finished_popup = get_node("../../../../../CongratulationsPopup")
	
func _on_Puzzle_pressed():
	if PuzzleAnswer.select:
		if PuzzleAnswer.selected.buttonid == self.buttonid:
			self.disabled = true
			self.set_disabled_texture(load("res://Assets/Puzzle/selected.png"))
			PuzzleAnswer.selected.disabled = true
			PuzzleAnswer.selected.set_disabled_texture(load("res://Assets/Puzzle/selected.png"))
			PuzzleAnswer.right += 1
			PuzzleAnswer.select = false
			if PuzzleAnswer.right == PuzzleAnswer.total_right:
				TaskCounts.taskDone += 1
				TaskCounts.Puzzle = true
				finished_popup.show()
		else:
			PuzzleAnswer.selected.disabled = false
			PuzzleAnswer.select = false
			self.disabled = false
			GlobalTimer.s -= 3
			if GlobalTimer.h <= 0 and GlobalTimer.m <= 0 and GlobalTimer.s <= 0:
				get_tree().change_scene(str("res://Scenes/7LosingScreen.tscn"))
			if GlobalTimer.s <= 0:
				GlobalTimer.m -= 1
				GlobalTimer.s = 57
	else:
		PuzzleAnswer.selected = self
		PuzzleAnswer.select = true
		self.disabled = true
		
	if GlobalTimer.h <= 0 and GlobalTimer.m <= 0 and GlobalTimer.s <= 0:
		get_tree().change_scene(str("res://Scenes/7LosingScreen.tscn"))
