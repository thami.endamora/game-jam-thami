extends TextureButton

func _on_ExitButton_pressed():
	setPosition()
	if TaskCounts.Good:
		get_tree().change_scene(str("res://Scenes/2Level.tscn"))
	else:
		get_tree().change_scene(str("res://Scenes/2LevelGood.tscn"))

func _on_OKButton_pressed():
	setPosition()
	if TaskCounts.Good:
		get_tree().change_scene(str("res://Scenes/2Level.tscn"))
	else:
		get_tree().change_scene(str("res://Scenes/2LevelGood.tscn"))

func setPosition():
	PlayerPosition.x = 225
	PlayerPosition.y = -1110
