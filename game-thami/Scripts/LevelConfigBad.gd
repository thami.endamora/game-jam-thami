extends TextureRect

var area2D
var areasignal

func _ready():
	TaskCounts.Nonogram = true
	var player = get_node("Player")
	player.set_global_position(Vector2(PlayerPosition.x, PlayerPosition.y))
	
	if GlobalTimer.h <= 0 and GlobalTimer.m <= 0 and GlobalTimer.s <= 0:
			get_tree().change_scene(str("res://Scenes/7LosingScreen.tscn"))

	if TaskCounts.TreasureChest:
		area2D = get_node("ChestTaskB")
		areasignal = get_node("ChestTaskB/GoToTaskB")
		area2D.remove_child(areasignal)

	if TaskCounts.Puzzle:
		area2D = get_node("FlagTaskC")
		areasignal = get_node("FlagTaskC/GoToTaskC")
		area2D.remove_child(areasignal)
	
	if TaskCounts.Star:
		area2D = get_node("StarQuestion")
		areasignal = get_node("StarQuestion/GoToStar")
		area2D.remove_child(areasignal)
	
