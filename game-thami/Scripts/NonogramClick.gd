extends TextureButton

export (bool) var click_block = false
var finished_popup

func _ready():
	finished_popup = get_node("../../../../../../CongratulationsPopup")

func _on_TextureButton_pressed():
	if click_block == true:
		set_normal_texture(load("res://Assets/Nonogram/blockblack.jpg"))
		NonogramAnswer.right_answer += 1
		self.disabled = true
		if GlobalTimer.h <= 0 and GlobalTimer.m <= 0 and GlobalTimer.s <= 0:
			get_tree().change_scene(str("res://Scenes/7LosingScreen.tscn"))
		if NonogramAnswer.right_answer == NonogramAnswer.total:
				TaskCounts.taskDone += 1
				TaskCounts.Nonogram = true
				finished_popup.show()

	if click_block == false:
		set_normal_texture(load("res://Assets/Nonogram/blockX.jpg"))
		GlobalTimer.s -= 3
		if GlobalTimer.h <= 0 and GlobalTimer.m <= 0 and GlobalTimer.s <= 0:
			get_tree().change_scene(str("res://Scenes/7LosingScreen.tscn"))
		if GlobalTimer.s <= 0:
			GlobalTimer.m -= 1
			GlobalTimer.s = 57
		
