extends Area2D

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		if TaskCounts.Good:
			if TaskCounts.taskDone == 2:
				GlobalTimer.m = 3
				GlobalTimer.s = 1
				get_tree().change_scene(str("res://Scenes/6WinningScreen.tscn"))
		else:
			if TaskCounts.taskDone == 1:
				GlobalTimer.m = 3
				GlobalTimer.s = 1
				get_tree().change_scene(str("res://Scenes/6WinningBad.tscn"))
