extends TextureButton

func _on_GoodButton_pressed():
	setPosition()
	TaskCounts.Star = true
	TaskCounts.Good = true
	get_tree().change_scene(str("res://Scenes/2Level.tscn"))

func _on_BadButton_pressed():
	setPosition()
	TaskCounts.Star = true
	TaskCounts.Good = false
	get_tree().change_scene(str("res://Scenes/2LevelGood.tscn"))

func setPosition():
	PlayerPosition.x = 923
	PlayerPosition.y = 200
