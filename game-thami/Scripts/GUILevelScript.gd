extends Node

var PopUpMenu

func _ready():
	PopUpMenu = get_node("BackMenuPopup")

func _on_TextureButton_pressed():
	PopUpMenu.show()

func _on_YesMenu_pressed():
	get_tree().change_scene(str("res://Scenes/1MainMenu.tscn"))
	GlobalTimer.m = 3
	GlobalTimer.s = 1
	TaskCounts.taskDone = 0
	TaskCounts.bonusDone = 0
	TaskCounts.Nonogram = false
	TaskCounts.TreasureChest = false
	TaskCounts.Puzzle = false
	TaskCounts.Star = false
	TaskCounts.Good = true

func _on_NoMenu_pressed():
	PopUpMenu.hide()
