# Lighthearted

In Lighthearted, join Pink, a hopeful spirit, in a quest to find happiness in the world. Pink has heard some rumors that information about true happiness can be found on the highest ground of HappyTown. But to open the portal, Pink needs to do some happiness task under 3 minutes before the portal gone forever! Will you help Pink finds the true happiness?


- Genre: Platformer + Puzzle
- Control: Keyboard (Arrow) + Mouse

```
Developed by Thami as a submission to Individual Game Jam CSUI 2020
Development Tools: Godot (GDScript) + Adobe Photoshop + Figma

```

